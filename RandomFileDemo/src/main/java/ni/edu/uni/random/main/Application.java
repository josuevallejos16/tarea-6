/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.random.main;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import ni.edu.uni.archivos.servicios.ActivoFijoService;
import ni.edu.uni.archivos.servicios.EmpleadoService;
import ni.edu.uni.random.dao.IDaoActivoFijo;
import ni.edu.uni.random.implement.ActivoFijoDaoImplement;
import ni.edu.uni.random.implement.EmpleadoDaoImplement;
import ni.edu.uni.random.model.ActivoFijoModel;
import ni.edu.uni.random.pojo.ActivoFijo;
import ni.edu.uni.random.pojo.Empleado;
import ni.edu.uni.random.pojo.TipoActivoFijo;

/**
 *
 * @author yasser.membreno
 */
public class Application {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        int opc;
        boolean flag1 = true, flag2 = true, flag3 = true;
        Scanner scan = new Scanner(System.in);
        ActivoFijoService afService = new ActivoFijoService(scan);
        EmpleadoService eService = new EmpleadoService(scan);
        do {
            menu();
            opc = scan.nextInt();

            switch (opc) {
                case 1:
                    do {
                        menuGAFijo();
                        opc = scan.nextInt();

                        switch (opc) {
                            case 1:
                            {
                                try {
                                    afService.create();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                                break;
                            case 2:
                            {
                                try {
                                    afService.update();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;
                            case 3:
                                boolean flag = afService.delete();
                                if (flag){
                                    System.out.println("\n\t-> Eliminacion Completada con exito");
                                }else{
                                    System.out.println("\n\t-> No se logro eliminar con exito");
                                }
                                break;
                            case 4:
                            {
                                try {
                                    System.out.println("\n============================================================================REGISTROS ENCONTRADOS==========================================================================");
                                    afService.findAll();
                                    System.out.println("=============================================================================================================================================================================\n");

                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;
                            case 5:
                                Optional<ActivoFijo> activoFijo = Optional.ofNullable(afService.findById());
                                if (activoFijo.isEmpty()){
                                    System.out.println("\n\tNo se encontro el activo fijo. ");
                                }else{
                                    System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
                                    afService.printHeaders();
                                    print(activoFijo.get());
                                    System.out.println("=============================================================================================================================================================================\n");
                                }
                                break;
                            case 6:

                                Optional<ActivoFijo[]> activoFijos = Optional.ofNullable(afService.findByClasificacion());

                                if (activoFijos.isPresent()){
                                    System.out.println("\n=============================================================================REGISTROS ENCONTRADOS===========================================================================");
                                    for (ActivoFijo activoFijo1:
                                            activoFijos.get()) {
                                        print(activoFijo1);
                                    }
                                    System.out.println("===========================================================================================================================================================================\n");

                                }else{
                                    System.out.println("\n\t-> No se encontraron coincidencias con este tipo de activo\n");
                                }
                                        
                                break;
                            case 7:
                            {
                                try {
                                    afService.asignarActivoFijo();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;
                            case 8:
                            {
                                try {
                                    afService.asignarActivosFijos();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                                break;
                            case 9:
                            {
                                try {
                                    afService.findActivosFijosByEmpleado();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                                break;
                            case 10:
                            {
                                try{
                                    afService.cambiarEstado ();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                                break;
                            case 11:
                                flag2 = false;
                                break;
                            default:
                                System.out.println("Opcion no valida!!");
                        }
                    } while (flag2);
                    break;
                case 2:
                    do {
                        menuGEmpleado();
                        opc = scan.nextInt();

                        switch (opc) {
                            case 1:
                            {
                                try {
                                    eService.create();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;

                            case 2:
                                //Actualizar
                                try {
                                    eService.update();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 3:
                                //Eliminar
                                try {
                                    eService.delete();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                break;
                            case 4:
                            {
                                try {
                                    System.out.println("\n==================================REGISTROS ENCONTRADOS==================================");
                                    eService.findAll();
                                    System.out.println("===========================================================================================");
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;

                            case 5:
                                    Optional<Empleado> empleado = Optional.ofNullable(eService.findById());
                                if (empleado.isEmpty()){
                                    System.out.println("\n\tNo se encontro el empleado. ");
                                }else{
                                    System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
                                    eService.printHeaders();
                                    print(empleado.get());
                                    System.out.println("=============================================================================================================================================================================\n");
                                }
                                break;
                            case 6:
                            {       
                                Optional<Empleado> Empleado = Optional.ofNullable(eService.findByCod());
                                if (Empleado.isEmpty()){
                                    System.out.println("\n\tNo se encontro el empleado. ");
                                }else{
                                    System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
                                    eService.printHeaders();
                                    print(Empleado.get());
                                    System.out.println("=============================================================================================================================================================================\n");
                                }
                            }    
                                break;
                            case 7:
                            {       
                                Optional<Empleado> Empleado = Optional.ofNullable(eService.findByCedula());
                                if (Empleado.isEmpty()){
                                    System.out.println("\n\tNo se encontro el empleado. ");
                                }else{
                                    System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
                                    eService.printHeaders();
                                    print(Empleado.get());
                                    System.out.println("=============================================================================================================================================================================\n");
                                }
                            }
                                break;
                            case 8:
                                Optional<Empleado> Empleado = Optional.ofNullable(eService.findByInss());
                                if (Empleado.isEmpty()){
                                    System.out.println("\n\tNo se encontro el empleado. ");
                                }else{
                                    System.out.println("\n=============================================================================REGISTRO ENCONTRADO===========================================================================");
                                    eService.printHeaders();
                                    print(Empleado.get());
                                    System.out.println("=============================================================================================================================================================================\n");
                                }
                                break;
                            case 9:
                            {
                                try {
                                    eService.findByFechaContratacion();
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                                break;
                            case 10:
                            {
                                try {
                                    eService.findBySalario();       
                                } catch (IOException ex) {
                                    Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            }
                            break;
                            case 11:
                                flag3 = false;
                                break;
                            default:
                                System.out.println("Opcion no es valida!!");
                        }
                    } while (flag3);
                    break;
                case 3:
                    System.out.println("Hasta luego!!");
                    flag1 = false;
                    break;
                default:
                    System.out.println("Opcion no es valida!!");
            }
        } while (flag1);
    }


    public static void menu() {
        System.out.println("Menu Principal");
        System.out.println("1. Gestionar Activos Fijos.");
        System.out.println("2. Gestionar Empleados.");
        System.out.println("3. Salir.");
    }

    public static void menuGAFijo() {
        System.out.println("Menu Gestion Activo Fijo");
        System.out.println("1. Agregar Activo fijo");
        System.out.println("2. Actualiar Activo fijo");
        System.out.println("3. Eliminar Activo fijo");
        System.out.println("4. Visualizar todos");
        System.out.println("5. Buscar por Id");
        System.out.println("6. Buscar por Clasificacion");
        System.out.println("7. Asignar activo fijo");
        System.out.println("8. Asignar todos los activos fijos");
        System.out.println("9. Buscar por empleado");
        System.out.println("10. Cambiar estado del activo fijo");
        System.out.println("11. Retornar menu principal");
    }

    public static void menuGEmpleado(){
        System.out.println("Menu Gestion de empleado");
        System.out.println("1. Agregar empleado");
        System.out.println("2. Actualizar empleado");
        System.out.println("3. Eliminar empleado");
        System.out.println("4. Visualizar todos");
        System.out.println("5. Buscar por id");
        System.out.println("6. Buscar por codigo empleado");
        System.out.println("7. Buscar por Cedula");
        System.out.println("8. Buscar por Inss");
        System.out.println("9. Buscar por Fecha contratacion");
        System.out.println("10. Buscar por salario");
        System.out.println("11. Retornar menu principal");
    }

    public static void subMenuTipoActivoFijo() {
        System.out.println("Seleccione una opcion");
        System.out.println("1. Vehiculo");
        System.out.println("2. Edificio");
        System.out.println("3. Equipo de Computo");
        System.out.println("4. Mobiliario");
    }
    
    public static void subMenuEstadoActivoFijo() {
        System.out.println("Seleccione una opcion");
        System.out.println("1. ASIGNADO");
        System.out.println("2. ACTIVO");
        System.out.println("3. MAL ESTADO");
        System.out.println("4. REPARACION");
        System.out.println("5. INACTIVO");
    }


    public static void print(ActivoFijo af) {
        System.out.format("%5d %20s %40s %20s %10.2f %10d %20s %20s %20s\n", af.getId(), af.getNombre(),
                af.getDescripcion(), af.getClasificacion(), af.getValor(), af.getCantidad(), af.getFechaCompra().toString(), af.getEstado(), (af.getEmpleado() == null) ? "empty" : af.getEmpleado().getNombres());
    }

    public static void print(Empleado e) {
        System.out.format("%5d %5d %20s %20s %20s %20s %20s %10.2f \n", e.getId(), e.getCodEmpleado(),
                e.getCedula(), e.getNombres(), e.getApellidos(), e.getInss(),
                e.getFechaContratacion().toString(), e.getSalario());
    }

}
